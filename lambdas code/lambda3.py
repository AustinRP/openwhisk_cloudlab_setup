import time
import boto3, json
import socket
import sys
import datetime
import random


def lambda_handler(event, context):
    # TODO implement
    loggingServerIp = "128.110.154.173"
    loggingServerPort = 10000
    sleeptimer = 25
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (loggingServerIp, loggingServerPort)
    randNum = random.randint(1,1000000000)
    # randNum = event['message']
    print('connecting to', server_address, 'port',sock.connect(server_address))
    try:
        # st2 = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S.%f')
        st2 = datetime.datetime.fromtimestamp(time.time()).strftime('%f')
        st = str(time.time()).split('.')
        message = "lambda3-start-"+st[0]+"."+st2+"-"+str(randNum)
        sock.sendall(message.encode())
    finally:
        sock.close()

    count = 0
    while count<5:
        print("Function 1 prints once 5 seconds.")
        count = count + 1
        time.sleep(sleeptimer/5) # Delay for 1 minute (60 seconds).
    invokeLam = boto3.client("lambda", region_name="us-east-1")
    payload = {"message": "Hi, you have been invoked."}

    #For InvocationType = "Event"
    # resp = invokeLam.invoke(FunctionName = "testlemda", InvocationType = "Event", Payload = json.dumps(payload))
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (loggingServerIp, loggingServerPort)
    print('connecting to', server_address, 'port',sock.connect(server_address))
    try:
        # st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S.%f')
        st2 = datetime.datetime.fromtimestamp(time.time()).strftime('%f')
        st = str(time.time()).split('.')
        message = "lambda3-end-"+st[0]+"."+st2+"-"+str(randNum)
        sock.sendall(message.encode())
    finally:
        sock.close()
    return {
        "statusCode": 200,
        "body": json.dumps('Hello from Lambda3!')
    }


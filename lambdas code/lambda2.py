import json
import time
import socket
import sys
import datetime
import random


def lambda_handler(event, context):
    # TODO implement
    # logging to loggingServer
    loggingServerIp = "128.110.154.173"
    loggingServerPort = 10000
    sleeptimer = 25
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (loggingServerIp, loggingServerPort)
    print('connecting to', server_address, 'port',sock.connect(server_address))
    randNum = random.randint(1,1000000000)
    # randNum = event['message']
    try:
        # st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S.%f')
        st2 = datetime.datetime.fromtimestamp(time.time()).strftime('%f')
        st = str(time.time()).split('.')
        message = "lambda2-start-"+st[0]+"."+st2+"-"+str(randNum)
        sock.sendall(message.encode())
    finally:
        sock.close()
    ##########################
    count = 0
    while count<5:
        print("Function 2 prints once 5 seconds.")
        count = count + 1
        time.sleep(sleeptimer/5) # Delay for 1 minute (60 seconds).
    print("function 2 ended!")
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (loggingServerIp, loggingServerPort)
    print('connecting to', server_address, 'port',sock.connect(server_address))
    try:
        # st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S.%f')
        st2 = datetime.datetime.fromtimestamp(time.time()).strftime('%f')
        st = str(time.time()).split('.')
        message = "lambda2-end-"+st[0]+"."+st2+"-"+str(randNum)
        sock.sendall(message.encode())
    finally:
        sock.close()
    return {
        "statusCode": 200,
        "body": json.dumps('Hello from Lambda2!')
    }

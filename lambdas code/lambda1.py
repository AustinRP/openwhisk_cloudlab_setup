import json
import sys
import datetime
import random
import boto3
import botocore


def lambda_handler(event, context):
    # TODO implement
    # logging to loggingServer
    loggingServerIp = "128.110.154.173"
    loggingServerPort = 10000
    sleeptimer = 25
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (loggingServerIp, loggingServerPort)
    randNum = random.randint(1,1000000000)
    print('connecting to', server_address, 'port',sock.connect(server_address))
    try:
        # st2 = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S.%f')
        st2 = datetime.datetime.fromtimestamp(time.time()).strftime('%f')
        st = str(time.time()).split('.')
        message = "lambda1-start-"+st[0]+"."+st2+"-"+str(randNum)
        sock.sendall(message.encode())
    finally:
        sock.close()
    ##########################
    count = 0
    while count<5:
        print("Function 1 prints once 5 seconds.")
        count = count + 1
        time.sleep(sleeptimer/5) # Delay for 1 minute (60 seconds).
    print("function 1 ended!")
    
    invokeLam = boto3.client("lambda", region_name="us-east-1")

    #For InvocationType = "Event"
    randNum2 = random.randint(1,1000000000)
    payload = {"message": str(randNum2)}
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (loggingServerIp, loggingServerPort)
    print('connecting to', server_address, 'port',sock.connect(server_address))
    try:
        # st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S.%f')
        st2 = datetime.datetime.fromtimestamp(time.time()).strftime('%f')
        st = str(time.time()).split('.')
        message = "lambda1-invokinglambda2-"+st[0]+"."+st2+"-"+str(randNum2)
        sock.sendall(message.encode())
    finally:
        sock.close()
    # resp = invokeLam.invoke(FunctionName = "testlembda2", InvocationType = "Event", Payload = json.dumps(payload))
    
    randNum2 = random.randint(1,1000000000)
    payload = {"message": str(randNum2)}
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (loggingServerIp, loggingServerPort)
    print('connecting to', server_address, 'port',sock.connect(server_address))
    try:
        # st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S.%f')
        st2 = datetime.datetime.fromtimestamp(time.time()).strftime('%f')
        st = str(time.time()).split('.')
        message = "lambda1-invokinglambda3-"+st[0]+"."+st2+"-"+str(randNum2)
        sock.sendall(message.encode())
    finally:
        sock.close()
    # resp2 = invokeLam.invoke(FunctionName = "testLambda3", InvocationType = "Event", Payload = json.dumps(payload))
    
    
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (loggingServerIp, loggingServerPort)
    print('connecting to', server_address, 'port',sock.connect(server_address))
    try:
        # st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S.%f')
        st2 = datetime.datetime.fromtimestamp(time.time()).strftime('%f')
        st = str(time.time()).split('.')
        message = "lambda1-end-"+st[0]+"."+st2+"-"+str(randNum)
        sock.sendall(message.encode())
    finally:
        sock.close()

    # files creation s3
    bucket_name = "serverless-lambda-openwhisk"
    stringcontent = "a"
    s3 = boto3.resource('s3')

    file_name = "/tem/"+str(randNum2)+".tempo"
    s3.Bucket(bucket_name).put_object(Key=file_name, Body=stringcontent.encode("utf-8"))
    file_name = "/tem/"+str(randNum2)+".tempo2"
    s3.Bucket(bucket_name).put_object(Key=file_name, Body=stringcontent.encode("utf-8"))

    return {
        "statusCode": 200,
        "body": json.dumps('Hello from Lambda1!')
    }



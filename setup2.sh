#!/bin/bash

if [ $USER != "root" ] ; then
    echo "Please run as root."
    exit
fi

sudo -u $SUDO_USER git clone https://github.com/apache/incubator-openwhisk-devtools.git
cd incubator-openwhisk-devtools/docker-compose
git checkout 14d918891571425cdb63f7aeb65a15dcf80c1c79
make quick-start

cp incubator-openwhisk-devtools/docker-compose/.wskprops /
cp incubator-openwhisk-devtools/docker-compose/openwhisk-src/bin/wsk /usr/local/bin/


echo "You should now have access to the wsk command"
